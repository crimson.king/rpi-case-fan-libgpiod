# https://www.gnu.org/software/make/manual/make.html#Makefile-Basics
SHELL = /bin/sh
INSTALL = install

prefix = /usr/local
datarootdir = $(prefix)/share
datadir = $(datarootdir)
exec_prefix = $(prefix)
bindir = $(exec_prefix)/bin
sysconfdir = $(prefix)/etc

all: build/rpi-case-fan

build/rpi-case-fan: rpi-case-fan.sh
	[ -d ./build ] || mkdir ./build
	cp ./rpi-case-fan.sh ./build/rpi-case-fan
	chmod +x ./build/rpi-case-fan

install: build/rpi-case-fan
	$(INSTALL) -m 755 -D \
		./build/rpi-case-fan $(DESTDIR)$(bindir)/

uninstall:
	rm -v \
		$(DESTDIR)$(bindir)/rpi-case-fan

clean:
	rm -v -r ./build

.PHONY: all install uninstall clean
