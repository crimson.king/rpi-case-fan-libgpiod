# Raspberry Pi Fan Control for Linux
[libgpiod](https://git.kernel.org/pub/scm/libs/libgpiod/libgpiod.git)-based shell script to drive the official [Raspberry Pi Case Fan](https://www.raspberrypi.com/products/raspberry-pi-4-case-fan/).

# How it works
The fan will start spinning when the GPU temperature is high (`-t TEMPERATURE`, in degrees Celsius), and it will continue to spin for a certain amount of time (`-d TIME`, in seconds).

```
rpi-case-fan [-t TEMPERATURE] [-d TIME] CHIP LINE
```

`CHIP` and `LINE` come from the output of `gpiocli detect` and `gpiocli info`, respectively.

## Requirements
- libgpiod CLI tools and DBus support *(tested on v2.2)*
- `vcgencmd` *(package `raspberrypi-utils-vcgencmd` on Alpine; last tested on v0.20241126-r0)*

## Installation
1. Run `make` and then `make install`.

2. Add a line to the system crontab:
```
*/1	*	*	*	*	/usr/local/bin/rpi-case-fan <chip> <line>
```
> This will run once every minute.

The script does not output anything under normal operation — cron won't bother you with emails.

## Development; Testing
Different temperature readings can be simulated by creating a mock `vcgencmd`.

1. Create the mock script and make it executable.
```sh
tee /usr/local/bin/vcgencmd-mock << EOI
#!/bin/sh

echo "temp=70.0'C"
EOI
chmod +x /usr/local/bin/vcgencmd-mock
```

2. Override the `VCGENCMD` environment variable when running `rpi-case-fan`.

```sh
VCGENCMD="vcgencmd-mock" rpi-case-fan
```
