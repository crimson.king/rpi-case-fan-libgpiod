#!/bin/sh
# libgpiod-based shell script to drive the official Raspberry Pi case fan.

threshold=70
duration=600
usage='rpi-case-fan [-t TEMPERATURE] [-d TIME] CHIP LINE'
vcgencmd="${VCGENCMD:-vcgencmd}"

args=`getopt -- t:d: "$@"`
if [ $? -ne 0 ]; then
	echo "$usage" >&2
	exit 2
fi
eval set -- "$args"
while true; do
	case "$1" in
		-t)
			threshold="$2"; shift; shift;;
		-d)
			duration="$2"; shift; shift;;
		--)
			shift; break;;
		*)
			echo "Error parsing options"; exit 1;;
	esac
done

if [ "$1" = "" ]; then
	echo "Specify the chip name (from 'gpiocli detect')" >&2
	exit 2
fi

if [ "$2" = "" ]; then
	echo "Specify the line number (from 'gpiocli info'; e.g. GPIO18 is number 18)" >&2
	exit 2
fi

chip="$1"
line="$2"
now=$(date '+%s')
consumer_prefix="rpi-case-fan"
run_dir="/var/run/rpi-case-fan"

read_temp() {
	temp=$($vcgencmd measure_temp | sed -r "s#temp=([0-9.]+)'C#\1#")
}

#
# Get or set the last time the fan was turned on.
#
spin_up() {
	if [ "$1" != "" ]; then
		echo "$1" | tee "$run_dir"/last_spin_up > /dev/null
	fi

	last_spin_up=$(cat "$run_dir"/last_spin_up)
}

#
# Change the fan state.
#
set_line() {
	gpiocli set --request="$request" "$line"="$1"
}


# Initialize /var/run
[ ! -d "$run_dir" ] && mkdir "$run_dir"
[ ! -f "$run_dir"/last_spin_up ] && spin_up "$now"

# Request the line
request=$(gpiocli requests | grep "$chip" | grep "$line")
if [ $? -ne 0 ]; then
	request=$(gpiocli request --output --chip="$chip" --consumer="$consumer_prefix" "$line")
	if [ $? -ne 0 ]; then
		echo "Failed to request the line"
		exit 1
	fi
else
	request=$(echo "$request" | awk '{print $1}')
fi

read_temp

hot=$(echo "$temp > $threshold" | bc)
spin_up

if [ "$hot" -eq 1 ]; then
	set_line 1
	spin_up "$now"
elif [ "$(( now - last_spin_up ))" -gt "$duration" ]; then
	# Will only turn it off after 10 minutes of operation
	set_line 0
fi

